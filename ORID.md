# ORID



## Objective

- In the morning, the teacher mainly made a summary of our speech on Cloud Native last week, so I have a deeper understanding of concepts such as microservices and containers.
- In addition, our group drew a concept map of spring boot together to make a summary of what we learned last week. At the same time, we did a review of HTML, CSS.
- In the afternoon, we got started with react, learned the basic use of react hooks, and did related exercises.



## Reflective

- Difficult 


## Interpretative

- Since I don't know much about front-end knowledge, it was a little difficult to learn.

## Decision

- I will continue to study hard in the next course and finish the homework on time.