import { TodoItem } from "../TodoItem";

export const TodoGroup = (props) => {
    const todoItems = props.todoList.map((item) => {
        return <TodoItem content={item} key={item.id} />
    });
    return (
        <div className="todoGroup">
            <h4>TodoList</h4>
            {todoItems}
        </div>
    )
}
