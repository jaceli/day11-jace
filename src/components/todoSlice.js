import { createSlice } from "@reduxjs/toolkit";

export const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todoList: [
        {
            id: "cc53dc26-61b0-406b-99dd-b8825dd2ceec",
            text: "todo example",
            done: false
        }, 
        {
            id: "dd53dc26-b061-6b40-dd99-82b85dd2ce90",
            text: "first todo item",
            done: true,
        }
    ]
    },
    reducers: {
        addNewTodo: (state, action) => {
            const todo = action.payload
            const newTodoList = [...state.todoList, todo]
            state.todoList = newTodoList 
        },
        deleteTodo: (state, action) => {
            const id = action.payload
            const newTodoList = state.todoList.filter((todo) => {
                return todo.id !== id
            })
            state.todoList = newTodoList
        },
        updateDone: (state, action) => {
            const id = action.payload
            const newTodoList = state.todoList.map((todo) => {
                if(todo.id === id) {
                    todo.done = !todo.done
                }
                return todo
            })
            state.todoList = newTodoList
        }
    }
})

export const {addNewTodo, deleteTodo, updateDone} = todoSlice.actions
export default todoSlice.reducer