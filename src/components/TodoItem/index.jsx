import { useDispatch } from 'react-redux'
import { deleteTodo, updateDone } from '../todoSlice'
import './index.css'


export const TodoItem = (props) => {
    const {content} = props
    const dispatch = useDispatch()

    const deleteOneTodo = (event) =>{
        event.stopPropagation()
        const id = props.content.id
        dispatch(deleteTodo(id))
    }

    return (
        <ul onClick={() => dispatch(updateDone(content.id))}>
            <li>
                <span style={{textDecoration: content.done?"line-through":"none"}}>{content.text}</span> 
                <button className='dtn' onClick={deleteOneTodo} >
                    X
                </button>
            </li>
        </ul>
    )
}
