// import './TodoList.css';
import { TodoGroup } from '../TodoGroup/';
import { TodoGenerator } from '../TodoGenerator/';
import { useSelector } from 'react-redux';

export const TodoList = () => {
    const todoListNew = useSelector(state => state.todo.todoList)

    return (
        <div>
            <TodoGroup todoList={todoListNew} />
            <TodoGenerator />
        </div>
    )
}
