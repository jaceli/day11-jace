import { useDispatch } from 'react-redux'
import './index.css'
import { useState } from 'react'
import { addNewTodo } from '../todoSlice'
import { nanoid } from '@reduxjs/toolkit'

export const TodoGenerator = () => {
    const [inputTodo, setInputTodo] = useState('')
    const dispatch = useDispatch()

    const addTodo = () => {
        if (inputTodo.trim() === '') {
            alert("请输入内容")
            return
        }
        const dic = { id: nanoid(), text: inputTodo, done: false }
        dispatch(addNewTodo(dic))
        setInputTodo('');
    }
    return (
        <div className="todo-header">
            <input type="text" value={inputTodo} placeholder='input a new todo here...' onChange={(event) => setInputTodo(event.target.value)} />
            <button style={{ width: "40px", height: "36px", marginLeft: "10px" }} onClick={addTodo}>add</button>
        </div>
    )
}
